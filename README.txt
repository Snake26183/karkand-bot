Ubuntu prereq setup:

sudo apt update
sudo apt install python3.9 -y
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python3.9 get-pip.py 
python3.9 -m pip install discord.py pymysql==0.9.3 aiomysql emoji glicko2 trueskill prettytable

Bot setup:

Edit Map_Bot.py in any IDE and add your owner ID at the top of the file and bot token at the bottom
Once the bot is invited and running do `!map_setup` to start

The bot won't create a new DB, so you need to use the blank one here, or use SQLite browser to clean up your old one

For public bot use: 
https://discord.com/api/oauth2/authorize?client_id=794712465002856449&permissions=8&scope=bot
