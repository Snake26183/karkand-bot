#-->******************* | Imports | ******************<--#
import re, sqlite3, discord, random
from os.path import isfile
import asyncio
guy_id =  #put your discord ID here
#database()   ##finish from 3 to finish
class Vote:
    sum_guy = 0
    icon = -1
Votes = []
Votess = []
class vote_new:
    def __init__(self, msg,cap_1,cap_2,now,count):
                self.msg = msg
                self.cap_1 = cap_1
                self.cap_2 = cap_2
                self.now = now
                self.count = count
vote = []
class Map_new:
    def __init__(self, guild_id, map_name,channel_id):
                self.guild_id = guild_id
                self.map_name = map_name
                self.channel_id = channel_id

Map_news = []
last_map = -1
last_last_map = -1
and_another_map = -1
client = discord.Client()
@client.event
async def on_ready():
    print('We have logged in as {0.user}'.format(client))
@client.event

async def on_message(message):
    global Votes
    global guy_id
    global Votess
    if ('!map_Null_fix' in message.content) and (message.author.bot == True or message.author.id == message.guild.owner_id or message.author.id == guy_id): 
        conn = sqlite3.connect("db_map_vote.db")
        c = conn.cursor()
        c.execute("UPDATE Guilds set channel_id == ? where Guild_id== ?", (message.channel.id, message.guild.id))
        c.execute("UPDATE Maps set channel_id == ? where Guild_id== ?", (message.channel.id, message.guild.id))
        conn.commit()
        c.close()
        conn.close()
    if ('!map_AB_emojis' in message.content) and (message.author.bot == True or message.author.id == message.guild.owner_id or message.author.id == guy_id): 
        conn = sqlite3.connect("db_map_vote.db")
        c = conn.cursor()
        sp = message.content.split(' ')
        c.execute("UPDATE Guilds set A_Team_Emoji == ?, B_Team_Emoji == ? where Guild_id== ? and channel_id == ?", (sp[1], sp[2], message.guild.id, message.channel.id))
        conn.commit()
        c.close()
        conn.close()
        await message.channel.send("Done")
    if ('!AB_Team_Names' in message.content) and (message.author.bot == True or message.author.id == message.guild.owner_id or message.author.id == guy_id): 
        conn = sqlite3.connect("db_map_vote.db")
        c = conn.cursor()
        sp = message.content.replace('!AB_Team_Names ','')
        sp = sp.split(',')
        c.execute("UPDATE Guilds set AB_Team_Names == ? where Guild_id== ? and channel_id == ?", (sp[0]+","+sp[1], message.guild.id, message.channel.id))
        conn.commit()
        c.close()
        conn.close()
        await message.channel.send("Done")
    if ('!Players_Per_Team' in message.content) and (message.author.bot == True or message.author.id == message.guild.owner_id or message.author.id == guy_id): 
        conn = sqlite3.connect("db_map_vote.db")
        c = conn.cursor()
        sp = message.content.split(' ')
        c.execute("UPDATE Guilds set Players_Per_Team == ? where Guild_id== ? and channel_id == ?", (int(sp[1]), message.guild.id, message.channel.id))
        conn.commit()
        c.close()
        conn.close()
        await message.channel.send("Done")
    if ('!Choose_one_map' in message.content): 
        captain_A = message.mentions[0]
        captain_B = message.mentions[1]
        if len(message.channel_mentions)>0:
            channel = message.channel_mentions[0].id
        else:channel = message.channel.id
        captain_R = random.randrange(0,2)
        if captain_R == 0:
            captain_R = captain_A
        else:captain_R = captain_B 
        conn = sqlite3.connect("db_map_vote.db")
        c = conn.cursor()
        class Map:
             def __init__(self, name, emoji,map_list):
                self.name = name
                self.emoji = emoji
                self.map_list = map_list
        Maps = []
        if c.execute('SELECT Map_name from Maps where Guild_ID == {0} and channel_id=={1}'.format(message.guild.id,channel)).fetchone() is None:
            await message.channel.send("Can't find any maps from your Guild in database, please write `!m_add_map [MAP_NAME] and then react to bot's message with prefered emoji for this map` example, `!m_add_map Hillside West` and react with 0️⃣ in any channel! For more info write !m_help")
            return
        else:
            c.execute('SELECT Map_name, Map_Emoji,Map_list from Maps where Guild_ID == {0} and channel_id=={1} order by map_list asc'.format(message.guild.id,channel))
        rows = c.fetchall()
        for row in rows:
            Maps.append(Map(row[0], row[1], row[2]))
        map_counter=5
        mda = []
        mes = ""
        mmm = ""
        for i in message.mentions:
            mes = mes + "<@{0}>".format(i.id)
            mmm += "\|<@{0}>".format(i.id)
            mmm += "\|"
        for i in Maps:
          if i.emoji[0] == '<':
              
                if map_counter>3:
                    mda.append('')
                    mda[len(mda)-1] = "" + "\n{0}- {1}".format(i.emoji, i.name)
                    map_counter = 0
                else:
                    mda[len(mda)-1] = mda[len(mda)-1] + "\n{0}- {1}".format(i.emoji, i.name)
                    map_counter+=1
          else:
            
                if map_counter>3:
                    mda.append('')
                    mda[len(mda)-1] = "" + "```prolog\n{0}- {1}```".format(i.emoji, i.name)
                    map_counter = 0
                else:
                    mda[len(mda)-1] = mda[len(mda)-1] + "```prolog\n{0}- {1}```".format(i.emoji, i.name)
                    map_counter+=1

        embed = discord.Embed(colour=discord.Colour(0x50e3c2), description="\|{0}".format(mmm))
        count = 0
        for i in mda:
            embed.add_field(name="**MAP LIST {0}**".format(count+1), value="{0}".format(i), inline=True)
            count += 1

        m = embed
        await asyncio.sleep(1)
        mes = "**                         <@{0}> BANS MAP**".format(captain_R.id)
        mm = await message.channel.send(content=mes,embed = m)
        Votess.append(vote_new(mm,captain_A,captain_B,captain_R,len(Maps)))
        for i in Maps:
                await mm.add_reaction(i.emoji)
                
        conn.close()
        return
    if ('map_set_list' in message.content) and (message.author.bot == True or message.author.id == message.guild.owner_id or message.author.id == guy_id): 
        m = await get_maps(message)
        if m is None:
            return
        else:
            aa = ""
            for i in m:
                aa=aa + "Map: |"+i.map_name+"|emoji: |"+i.map_emoji+"|list: |"+str(i.map_list)+"\n"
            mes = await message.channel.send("list_set {0} Here is map list: please choose where to use dat list number.\n".format(message.content.split(' ')[1]) + aa)
            for i in m:
                await mes.add_reaction(i.map_emoji)
            await mes.add_reaction('❌')
    if ('**TEAMS READY:**' in message.content) and (message.author.bot == True or message.author.id == message.guild.owner_id or message.author.id == guy_id): 
        conn = sqlite3.connect("db_map_vote.db")
        c = conn.cursor()
        okeeee = c.execute('SELECT A_Team_Emoji from Guilds where Guild_ID == {0} and channel_id=={1}'.format(message.guild.id, message.channel.id)).fetchone()[0]
        if ':' not in okeeee:
            ok="OKEEE"
        else:ok = ':'+okeeee.split(':')[1]+':'
        reg = re.compile(r"\*\*TEAMS READY:\*\*+\s+"+ok)
        r = re.search(reg, message.content)
        if r is None:
                reg = re.compile(r"\*\*TEAMS READY:\*\*+\s+"+okeeee)
                r = re.search(reg, message.content)
                if r is None:
                    reg = re.compile(r"pickup has been started!+\s+"+okeeee)
                    r = re.search(reg, message.content)
                    if r is None:
                        reg = re.compile(r"pickup has been started!+\s+"+ok)
                        r = re.search(reg, message.content)
                        if r is None:
                            r = 0
                        else:r = 1
                    else:r = 1
                else:r = 1
        else:r = 1
        if len(message.mentions) < 1:
            await message.channel.send("```0 MENTIONS FOUND, CAN'T CALL MAP VOTE!```")
            return
        Votes.append(Vote)
        Votes[len(Votes)-1].sum_guy = message.mentions[0].id
        Votes[len(Votes)-1].icon = r
        conn = sqlite3.connect("db_map_vote.db")
        c = conn.cursor()
        seconds = c.execute('SELECT timer from Guilds where Guild_ID == {0} and channel_id=={1}'.format(message.guild.id,message.channel.id)).fetchone()[0]
        guild = message.guild.id
        if c.execute('SELECT Guild_ID from Guilds where Guild_ID == {0} and channel_id=={1}'.format(guild,message.channel.id)).fetchone() is None:
            await message.channel.send("Can't find your Guild in database, please write !m_setup in any channel! For more info write !map_help")
            return
        Banned_maps = c.execute('SELECT BannedMaps from Guilds where Guild_ID == {0} and channel_id=={1}'.format(guild,message.channel.id)).fetchone()[0]
        Banned_maps = Banned_maps.split(',')
        mes = ""
        mda = []
        mmm = ""
        for i in message.mentions:
            mes = mes + "<@{0}>".format(i.id)
            mmm += "\|<@{0}>".format(i.id)
            mmm += "\|"
        class Map:
             def __init__(self, name, emoji,map_list):
                self.name = name
                self.emoji = emoji
                self.map_list = map_list
        Maps = []
        if c.execute('SELECT Map_name from Maps where Guild_ID == {0} and channel_id=={1}'.format(guild,message.channel.id)).fetchone() is None:
            await message.channel.send("Can't find any maps from your Guild in database, please write `!m_add_map [MAP_NAME] and then react to bot's message with prefered emoji for this map` example, `!m_add_map Hillside West` and react with 0️⃣ in any channel! For more info write !m_help")
            return
        else:
            c.execute('SELECT Map_name, Map_Emoji,Map_list from Maps where Guild_ID == {0} and channel_id=={1} order by map_list asc'.format(guild,message.channel.id))
        rows = c.fetchall()
        for row in rows:
            Maps.append(Map(row[0], row[1], row[2]))
        map_counter=5
        for i in Maps:
          if i.emoji[0] == '<':
              if i.name in Banned_maps:
                if map_counter>3:
                    mda.append('')
                    mda[len(mda)-1] = "" + "```ml\n📛- {0}```".format(i.name)
                    map_counter = 0
                else:
                    mda[len(mda)-1] = mda[len(mda)-1] + "```ml\n📛- {0}```".format(i.name)
                    map_counter+=1
              else:
                if map_counter>3:
                    mda.append('')
                    mda[len(mda)-1] = "" + "\n{0}- {1}".format(i.emoji, i.name)
                    map_counter = 0
                else:
                    mda[len(mda)-1] = mda[len(mda)-1] + "\n{0}- {1}".format(i.emoji, i.name)
                    map_counter+=1
          else:
            if i.name in Banned_maps:
                if map_counter>3:
                    mda.append('')
                    mda[len(mda)-1] = "" + "```ml\n📛- {0}```".format(i.name)
                    map_counter = 0
                else:
                    mda[len(mda)-1] = mda[len(mda)-1] + "```ml\n📛- {0}```".format(i.name)
                    map_counter+=1
            else:
                if map_counter>3:
                    mda.append('')
                    mda[len(mda)-1] = "" + "```prolog\n{0}- {1}```".format(i.emoji, i.name)
                    map_counter = 0
                else:
                    mda[len(mda)-1] = mda[len(mda)-1] + "```prolog\n{0}- {1}```".format(i.emoji, i.name)
                    map_counter+=1

##
        embed = discord.Embed(colour=discord.Colour(0x50e3c2), description="\|{0}".format(mmm))
        count = 0
        for i in mda:
            embed.add_field(name="**MAP LIST {0}**".format(count+1), value="{0}".format(i), inline=True)
            count += 1

        m = embed
        await asyncio.sleep(1)
        await message.channel.send(content=mes+"**                                  VOTE For A Map**",embed = m)
        emoji_timer = ""
        for x in range(len(str(seconds))):
            emoji_timer = emoji_timer + numbers(int(str(seconds)[x]))
        await message.channel.send("```md\n#Timer started! ---> |{0}  Seconds left\n```".format(emoji_timer))
        c.close()
        conn.close()
    if '!test' in message.content and (message.author.id == message.guild.owner_id or message.author.bot == True or message.author.id == guy_id): 
        await message.channel.send("<a:Countingdown:714073632858701844>")
    if 'Timer started!' in message.content and (message.author.id == message.guild.owner_id or message.author.bot == True or message.author.id == guy_id): 
        conn = sqlite3.connect("db_map_vote.db")
        c = conn.cursor()

        seconds = c.execute('SELECT timer from Guilds where Guild_ID == {0} and channel_id == {1}'.format(message.guild.id,message.channel.id)).fetchone()[0]
        timer = seconds/2
        and_sec = str(seconds)[len(str(seconds))-1]
        and_sec = int(and_sec)
        temp = ""
        for x in range(len(str(seconds))-1):
            temp = str(seconds)[x]
        seconds = int(temp)
        mm = message.content
        while timer >= 0:
            and_sec -= 2
            if and_sec <= -1:
                seconds -= 1
                and_sec = 9
            await asyncio.sleep(2)
            mm = "```md\n#Timer started! ---> |"+numbers(seconds)+""+numbers(and_sec)+"| Seconds left\n```"

            timer -= 1
            await message.edit(content=str(mm))
        await message.delete()
    if ('!map_setup' in message.content) and (message.author.id == message.guild.owner_id or message.author.id == guy_id):
        conn = sqlite3.connect("db_map_vote.db")
        c = conn.cursor()
        if c.execute('SELECT Guild_ID from Guilds where Guild_ID == {0} and channel_id == {1}'.format(message.guild.id,message.channel.id)).fetchone() is not None:
            await message.channel.send("Already Registered this Guild!")
            return
        else:
            c.execute('INSERT INTO Guilds (Guild_id, Timer, Archive_ID,channel_id) VALUES(?, ?, ?,?)', (message.guild.id,60,0,message.channel.id))
            await message.channel.send("```Added your Guild to my system, now do this:\n 1. To add maps: !add_map MAP_NAME   then bot will show message, react to it with emoji that you want to use for this map. if you want to delete map, !del_map then bot will how list of maps with emojis react with map's emoji \n2. !map_list to show curent map list\n3.To change timer, write: !map_timer numbers(in seconds) \n(Optionally) to add map voting archive(if you want to keep track of who voted for what) !archive_id #ARCHIVE_CHANNEL\n4.to delete archive, write: !archive_del (just makes it so that bot don't send message).\n5. if you want to not use last_map or last_last_map, just write !last_map 0 to turn off last map write !last_map_d to turn off last last map and last map off, to turn back on: !last_map_e .```")
            
            conn.commit()
            c.close()
            conn.close()
    if '!add_map' in message.content and (message.author.id == message.guild.owner_id or message.author.id == guy_id):
        global Map_news
        message_txt = message.content.split(" ")
        if len(message_txt) <= 1:
            await message.channel.send("Write map name!")
            return
        else:
            mv = ""
            for x in range(len(message_txt)-1):
                mv=mv+ message_txt[x+1]
                if x < (len(message_txt)-2):
                    mv +=" "

            Map_news.append(Map_new(message.guild.id, mv,message.channel.id))

            await message.channel.send("**React on me with emoji that you want to use for: **`{0}` , react with `❌` to cancel".format(mv))
    if ('!del_map' in message.content) and (message.author.id == message.guild.owner_id or message.author.id == guy_id):
        m = await get_maps(message)
        if m is None:
            return
        else:
            aa = ""
            for i in m:
                aa=aa + "Map: "+i.map_name+" emoji: "+i.map_emoji+"\n"
            mes = await message.channel.send("you want to delete...: (react with `❌` to cancel)\n" + aa)
            for i in m:
                await mes.add_reaction(i.map_emoji)
            await mes.add_reaction('❌')
    if ('!map_cfg' in message.content) and (message.author.id == message.guild.owner_id or message.author.id == guy_id):
        conn = sqlite3.connect("db_map_vote.db")
        c = conn.cursor()
        rows = c.execute('SELECT * from Guilds where Guild_ID == {0} and channel_id=={1}'.format(message.guild.id, message.channel.id)).fetchall()
        txt = ""
        for i in rows:
            txt += "|Guild_ID:"+str(i[0])+"|\n|"+"Archive_ID:"+str(i[1])+"|\n|"+"Timer:"+str(i[2])+"|\n|"+"Channel_ID:"+str(i[3])+"|\n|"+"Players_Per_Team:"+str(i[4])+"|\n|"+"A_Team_Emoji:"+str(i[5])+"|\n|"+"B_Team_Emoji:"+str(i[6])+"|\n|"+"AB_Team_Names:"+str(i[7])+"|\n|"+"BannedMaps:"+str(i[8])+"|\n|"+"Overall_Played:"+str(i[9])+"|"
        await message.channel.send("`{0}`".format(txt))
        c.close()
        conn.close()
    if ('!map_played' in message.content) and (message.author.id == message.guild.owner_id or message.author.id == guy_id):
        m = await get_maps(message)
        if m is None:
            return
        else:
            aa = ""
            for i in m:
                aa=aa + "Map: |"+i.map_name+"|emoji: |"+i.map_emoji+"|list: |"+str(i.map_list)+"\n"
            mes = await message.channel.send("Here is map list: please choose where to check Ratio.\n{0}".format(aa))
            for i in m:
                await mes.add_reaction(i.map_emoji)
            await mes.add_reaction('❌')
    if ('!map_list' in message.content) and (message.author.id == message.guild.owner_id or message.author.id == guy_id):
        m = await get_maps(message)
        if m is None:
            return
        else:
            aa = ""
            conn = sqlite3.connect("db_map_vote.db")
            c = conn.cursor()
            overall_maps = c.execute('SELECT Overall_Played from Guilds where Guild_ID == {0} and channel_id=={1}'.format(message.guild.id, message.channel.id)).fetchone()[0]
            if overall_maps == 0: overall_maps = -1
            c.close()
            conn.close()
            for i in m:
                prcnt = int(float(i.map_ratio)/float(overall_maps)*100.0)
                map_txt = "[{0}/{1}]({2}%)".format(i.map_ratio,overall_maps,str(prcnt))
                aa=aa + "Map: |"+i.map_name+"|emoji: |"+i.map_emoji+"|list: |"+str(i.map_list)+"|Played:`"+str(map_txt)+"`\n"

            mes = await message.channel.send("Here is map list: \n" + aa)
    if ('!map_timer' in message.content) and (message.author.id == message.guild.owner_id or message.author.id == guy_id):
        message_txt = message.content.split(" ")
        if len(message_txt) <= 1:
            await message.channel.send("Write time (in seconds)!")
            return
        else:
            conn = sqlite3.connect("db_map_vote.db")
            c = conn.cursor()
            c.execute("UPDATE Guilds set timer == ? where Guild_id== ? and channel_id =?", (int(message_txt[1]), message.guild.id,message.channel.id))
            conn.commit()
            c.close()
            conn.close()
            await message.channel.send("done, timer is: "+message_txt[1]+"seconds now")
    if ('!archive_id' in message.content) and (message.author.id == message.guild.owner_id or message.author.id == guy_id):
        message_txt = message.content.split(" ")
        if len(message.channel_mentions) == 0:
            await message.channel.send("Please # your archive channel")
            return
        else:
            conn = sqlite3.connect("db_map_vote.db")
            c = conn.cursor()
            c.execute("UPDATE Guilds set Archive_id = ? where Guild_id=? and channel_id=?", (message.channel_mentions[0].id,message.guild.id, message.channel.id))
            conn.commit()
            c.close()
            conn.close()
            await message.channel.send("done,archive is: "+"<#"+str(message.channel_mentions[0].id)+"> now plz don't forget to give me access to this channel :) (read and write)")
    if ('!archive_del' in message.content) and (message.author.id == message.guild.owner_id or message.author.id == guy_id):
            conn = sqlite3.connect("db_map_vote.db")
            c = conn.cursor()
            c.execute("UPDATE Guilds set Archive_id = ? where Guild_id=? and channel_id=?", (int(0), message.guild.id,message.channel.id))
            conn.commit()
            c.close()
            conn.close()
            await message.channel.send("done, archive is deleted, to make one again write: !archive_add")
    if ('!last_map_e' in message.content) and (message.author.id == message.guild.owner_id or message.author.id == guy_id):
            conn = sqlite3.connect("db_map_vote.db")
            c = conn.cursor()
            msg = int(message.content.split(' ')[1])
            if msg < 0:
                return
            okay = ""
            if msg == 0:okay = "Null"
            else:
                Banned_maps = c.execute('SELECT BannedMaps from Guilds where Guild_ID == {0} and channel_id=={1}'.format(message.guild.id,message.channel.id)).fetchone()[0]
                Banned_maps = Banned_maps.split(',')
                blen = len(Banned_maps)
                for i in range(msg-1):
                    if i >= blen:okay +="Null"+","
                    else:okay+=Banned_maps[i]+","
                
                if msg >= blen:okay +="Null"
                else:okay+=Banned_maps[msg-1]

            c.execute("UPDATE Guilds set BannedMaps=? where Guild_id=? and channel_id=?", (okay, message.guild.id,message.channel.id))
            conn.commit()
            c.close()
            conn.close()
            await message.channel.send("done, last_map enabled for "+str(msg)+" maps,right now banned maps are:`"+okay+"`")
    if 'VOTE For A Map**' in message.content and (message.author.id == message.guild.owner_id or message.author.bot == True or message.author.id == guy_id): 
        mentionss = message.mentions
        side = -1
        for i in Votes:
            if mentionss[0].id == i.sum_guy:
                side = i.icon
                Votes.remove(i)
                break
        conn = sqlite3.connect("db_map_vote.db")
        c = conn.cursor()

        seconds = c.execute('SELECT timer from Guilds where Guild_ID == {0} and channel_id=={1}'.format(message.guild.id,message.channel.id)).fetchone()[0]
        await message.edit(content=str("**                                  VOTE For A Map**"))
        maps = []
        class Map:
             def __init__(self, name, emoji):
                self.name = name
                self.emoji = emoji
        Maps = []
        emojis_check = []
        mm = "\n"
        Banned_maps = c.execute('SELECT BannedMaps from Guilds where Guild_ID == {0} and channel_id=={1}'.format(message.guild.id,message.channel.id)).fetchone()[0]
        Banned_maps = Banned_maps.split(',')
        c.execute('SELECT Map_name, Map_Emoji from Maps where Guild_ID == {0} and channel_id=={1} order by map_list asc'.format(message.guild.id, message.channel.id))
        rows = c.fetchall()
        for row in rows:
            Maps.append(Map(row[0], row[1]))
        for i in Maps:
            if i.name not in Banned_maps:
                await message.add_reaction(i.emoji)
                maps.append(i.name)
                if i.emoji[0] == "<":
                    reg = re.compile(r"\d{18}")
                    r = re.search(reg, i.emoji)
                    r = int(r.group(0))
                    emojis_check.append(client.get_emoji(r))
                    
                else:
                    emojis_check.append(i.emoji)
        
        
        c.close()
        conn.close()
        await asyncio.sleep(seconds+1) #####################################
        max = 0
        maxxx=[]
        count = 0
        last_max = 0
        kk=0
        mm = mm + "People,who can vote are: "
        for i in mentionss:
            mm=mm+"|"+i.display_name+"|"
        mm = mm + "\n----------------------------------------------\n"
        for n in message.reactions:
            for o in emojis_check:
                if o == n.emoji:
                    users = await n.users().flatten()
                    new_count = 0
                    for user in users:
                        for i in mentionss:
                            if user.id==i.id:
                                new_count +=1
                                mm = mm + "|"+user.display_name+"| "
                    if new_count > last_max:
                        max = count
                        last_max = new_count
                    if new_count != 0:
                        kk+=1
                        new = "<"
                        o = str(o)
                        mm = mm + "voted for: "+o+"\t"+ "In total: ["+str(new_count)+"] votes for "+o+"which is: **"+maps[count]+"**\n"
                    count+=1
        count = 0
        for n in message.reactions:
            for o in emojis_check:
                if o == n.emoji:
                    users = await n.users().flatten()
                    new_count = 0
                    for user in users:
                        for i in mentionss:
                            if user.id==i.id:
                                new_count +=1
                    if new_count == last_max:
                        maxxx.append(count)
                    count+=1
        conn = sqlite3.connect("db_map_vote.db")
        c = conn.cursor()
        Banned_maps = c.execute('SELECT BannedMaps from Guilds where Guild_ID == {0} and channel_id=={1}'.format(message.guild.id,message.channel.id)).fetchone()[0]
        Banned_maps = Banned_maps.split(',')
        c.close()
        conn.close()
        
        if len(maxxx)>1 or kk == 0:
            max = maxxx[random.randrange(-1, len(maxxx))]
        if len(Banned_maps) == 1 and Banned_maps[0] == "Null":
            new_Banned_Maps = "Null"
        else:
            new_Banned_Maps = ""
            new_banned_len = range(len(Banned_maps)-1)
            for p in new_banned_len:
                new_Banned_Maps+=Banned_maps[p+1]+","
            new_Banned_Maps+=maps[max]
        mm = mm + "\n `Map is: {0}`\n".format(maps[max])
        conn = sqlite3.connect("db_map_vote.db")
        c = conn.cursor()
        response = c.execute('SELECT Archive_ID from Guilds where Guild_ID == {0} and channel_id=={1}'.format(message.guild.id, message.channel.id)).fetchone()[0]
        if response != 0:
            await message.guild.get_channel(int(response)).send(mm)
        #await message.channel.send("Map is: **{0}**".format(maps[max]))
        c.execute('UPDATE Guilds SET BannedMaps = ? where Guild_id= ? and channel_id==?', (new_Banned_Maps, message.guild.id,message.channel.id))
        map_picked = c.execute('SELECT Played_Times from Maps where Guild_ID = {0} and channel_id={1} and Map_Name=\'{2}\''.format(message.guild.id, message.channel.id,maps[max])).fetchone()[0]
        c.execute('UPDATE Maps SET Played_Times = ? where Guild_id== ? and channel_id==? and Map_Name == ?', (int(map_picked)+1, message.guild.id,message.channel.id,maps[max]))
        overall_maps = c.execute('SELECT Overall_Played from Guilds where Guild_ID == {0} and channel_id=={1}'.format(message.guild.id, message.channel.id)).fetchone()[0]
        c.execute('UPDATE Guilds SET Overall_Played = ? where Guild_id== ? and channel_id==?', (int(overall_maps)+1, message.guild.id,message.channel.id))
        map_picked += 1
        overall_maps += 1
        prcnt = int(float(map_picked)/float(overall_maps)*100.0)
        map_txt = "[{0}/{1}]({2}%)".format(map_picked,overall_maps,str(prcnt))
        players_per_team = c.execute('SELECT "Players_Per_Team" from Guilds where Guild_ID == {0} and channel_id=={1}'.format(message.guild.id, message.channel.id)).fetchone()[0]
        alpha_side_emoji = c.execute('SELECT A_Team_Emoji from Guilds where Guild_ID == {0} and channel_id=={1}'.format(message.guild.id, message.channel.id)).fetchone()[0]
        bravo_side_emoji = c.execute('SELECT B_Team_Emoji from Guilds where Guild_ID == {0} and channel_id=={1}'.format(message.guild.id, message.channel.id)).fetchone()[0]
        alpha_side_name = c.execute('SELECT AB_Team_Names from Guilds where Guild_ID == {0} and channel_id=={1}'.format(message.guild.id, message.channel.id)).fetchone()[0] #AB_Team_Names
        c.close()
        conn.commit()
        conn.close()
        bravo_side_name = alpha_side_name.split(',')[1]
        alpha_side_name = alpha_side_name.split(',')[0]
        embed = discord.Embed(colour=discord.Colour(0x856fef))
        alpha_side = ""
        bravo_side = ""
        if side == 1:
            alpha_side = alpha_side_emoji
            alpha_name = alpha_side_name
            bravo_side = bravo_side_emoji
            bravo_name = bravo_side_name
        else: 
            alpha_side = bravo_side_emoji
            bravo_side = alpha_side_emoji
            alpha_name = bravo_side_name
            bravo_name = alpha_side_name
        number13 = 0

        alpha_players=""
        bravo_players=""
        for i in mentionss:
            if number13<players_per_team:
               alpha_players+="{0}\n".format(i.display_name)
            else: bravo_players +="{0}\n".format(i.display_name)
            number13+=1
        wh = ""
        wh2 = ""
        if side == 1:
            wh = "glsl"
            wh2 = "md"
        else: 
            wh="md"
            wh2="glsl"
        embed.add_field(name="`    [`{0}`] USMC [`{0}`]   |`".format(alpha_side), value="```{2}\n#{0:^20}``````fix\n{1}```".format(alpha_name,alpha_players,wh), inline=True)
        embed.add_field(name="`   [`{0}`] MEC/PLA [`{0}`]   `".format(bravo_side), value="```{2}\n#{0:^18}``````fix\n{1}```".format(bravo_name,bravo_players,wh2), inline=True)
        embed.add_field(name="\u200B", value="```fix\n{0:^38}\n{1:^38}```".format("Map: "+maps[max],map_txt), inline=False)
        await message.channel.send(embed = embed)

        await message.delete()
@client.event
async def on_reaction_add(reaction, user):
    global guy_id
    message = reaction.message
    if ('Here is map list: please choose where to check Ratio.' in reaction.message.content and (user.id == reaction.message.guild.owner_id or user.id == guy_id)):
        if reaction.emoji == "❌":
           await reaction.message.delete()
           return
        else:
       
            m = await get_maps(reaction.message)
            if m is None:
                return
            conn = sqlite3.connect("db_map_vote.db")
            c = conn.cursor()
            map_picked = c.execute('SELECT Played_Times from Maps where Guild_ID = {0} and channel_id={1} and Map_Name=\'{2}\''.format(message.guild.id, message.channel.id,m[0].map_name)).fetchone()[0]
            overall_maps = c.execute('SELECT Overall_Played from Guilds where Guild_ID == {0} and channel_id=={1}'.format(message.guild.id, message.channel.id)).fetchone()[0]
            if overall_maps == 0: overall_maps = -1
            prcnt = int(float(map_picked)/float(overall_maps)*100.0)
            map_txt = "[{0}/{1}]({2}%)".format(map_picked,overall_maps,str(prcnt))
            c.close()
            conn.close()
            await reaction.message.channel.send("```fix\n{0} | {1}```".format("Map: "+m[0].map_name,map_txt))
            await reaction.message.delete()

    if ('you want to delete...:' in reaction.message.content and (user.id == reaction.message.guild.owner_id or user.id == guy_id)):
        if reaction.emoji == "❌":
            await reaction.message.channel.send("canceling...!")
            await reaction.message.delete()
        else:
            m = await get_maps(reaction.message)
            if m is None:
                return
            else:
                k= 0
                for i in m:
                    p = ""
                    if i.map_emoji[0] == "<":
                        reg = re.compile(r"\d{18}")
                        r = re.search(reg, i.map_emoji)
                        r = int(r.group(0))
                        p = client.get_emoji(r)
                    
                    else:
                        p = i.map_emoji

                    if p == reaction.emoji:
                        k = i
                        mes = await reaction.message.channel.send("deleting map:" + i.map_name)
                        conn = sqlite3.connect("db_map_vote.db")
                        c = conn.cursor()
                        c.execute('DELETE FROM Maps WHERE Map_name == ? and guild_id == ? and channel_id==?',(i.map_name,reaction.message.guild.id,reaction.message.channel.id))
                        conn.commit()
                        c.close()
                        conn.close()
                if k == 0:
                    await reaction.message.channel.send("couldn't detect map using this emoji!")
                    await reaction.message.delete()
                else:await reaction.message.delete()
                
    if ('BANS MAP**' in reaction.message.content and (user.id == reaction.message.mentions[0].id)):
        global Votess
        msg = reaction.message
        vv = ""
        for i in Votess:
            if i.msg.id == msg.id:
                vv = i
        for i in msg.reactions:
            if i == reaction:
                mmm = msg.embeds[0].description
                embed = discord.Embed(colour=discord.Colour(0x50e3c2), description=mmm)
                for n in msg.embeds[0]._fields:
                    embed.add_field(name="{0}".format(n['name']), value="{0}".format(n['value'].replace(reaction.emoji,'📛')), inline=True)
                if vv.now == vv.cap_1: vv.now =  vv.cap_2
                else:vv.now =  vv.cap_1
                m = embed
                await msg.edit(content="**                         <@{0}> BANS MAP**".format(vv.now.id),embed = m)
                await reaction.clear()
                await asyncio.sleep(0.5)
                if len(msg.reactions) == 1:
                    okes = []
                    for n in msg.embeds[0]._fields:
                        okes = n['value'].split('\n')
                        choose = ""
                        for o in okes:
                            if msg.reactions[0].emoji in o:
                                choose = o.replace(msg.reactions[0].emoji,'').replace('``````prolog','')
                    
                    await msg.channel.send("```Prolog\nMAP WILL BE: {0}```".format(choose))
                    await msg.delete()

    if ('**React on me with emoji that you want to use for: **' in reaction.message.content and (user.id == reaction.message.guild.owner_id or user.id == guy_id)):
        if reaction.emoji == "❌":
            global Map_news
            await reaction.message.channel.send("canceling...!")
            map_this = ""
            for i in Map_news:
                if i.guild_id == reaction.message.guild.id and i.channel_id == reaction.message.channel.id:
                    Map_news.remove(i)
            await reaction.message.delete()
        else:
            map_this = ""
            for i in Map_news:
                if i.guild_id == reaction.message.guild.id and i.channel_id == reaction.message.channel.id:
                    map_this = i.map_name
            
            conn = sqlite3.connect("db_map_vote.db")
            c = conn.cursor()
            if reaction.custom_emoji == True:
                test = "<"
                if reaction.emoji.animated == True:
                    test+="a"
                test += ":{0}:{1}>".format(reaction.emoji.name,reaction.emoji.id)
            else: test = reaction.emoji
            test = c.execute('select * from Maps where map_emoji == ? and guild_id == ? and channel_id==?', (test, reaction.message.guild.id, reaction.message.channel.id)).fetchone()
            if test is not None:
                await reaction.message.channel.send("This emoji is already used by other map!")
                return
            
            test = 1
            if reaction.custom_emoji == True:
                emoji = "<"
                if reaction.emoji.animated == True:
                    emoji+="a"
                emoji += ":{0}:{1}>".format(reaction.emoji.name,reaction.emoji.id)
            else: emoji = reaction.emoji
            c.execute('INSERT INTO Maps (Map_name, map_emoji, map_list, guild_id, channel_id) VALUES(?, ?, ?,?,?)', (map_this,emoji,test,reaction.message.guild.id,reaction.message.channel.id))
            conn.commit()
            c.close()
            conn.close()
            await reaction.message.channel.send("done!")
            map_this = ""
            for i in Map_news:
                if i.guild_id == reaction.message.guild.id:
                    Map_news.remove(i)
            await reaction.message.delete()
    if ('Here is map list: please choose where to use dat list number.' in reaction.message.content and (user.id == reaction.message.guild.owner_id or user.id == guy_id)):
        if reaction.emoji == "❌":
           await reaction.message.delete()
        else:
       
            m = await get_maps(reaction.message)
            if m is None:
                return
            else:
                k= 0
                for i in m:
                    p = ""
                    if i.map_emoji[0] == "<":
                        reg = re.compile(r"\d{18}")
                        r = re.search(reg, i.map_emoji)
                        r = int(r.group(0))
                        p = client.get_emoji(r)
                    
                    else:
                        p = i.map_emoji

                    if p == reaction.emoji:
                        k = i
                        mes = await reaction.message.channel.send("changing list for a map:" + i.map_name)
                        conn = sqlite3.connect("db_map_vote.db")
                        c = conn.cursor()
                        llll = int(reaction.message.content.split(" ")[1])
                        c.execute('update Maps set map_list == ? where Map_name == ? and guild_id == ? and channel_id == ?',(llll, i.map_name,reaction.message.guild.id,reaction.message.channel.id))
                        conn.commit()
                        c.close()
                        conn.close()
                if k == 0:
                    await reaction.message.channel.send("couldn't detect map using this emoji!")
                    await reaction.message.delete()
                else:await reaction.message.delete()
            
def numbers(num):
    mmm = ""
    if num == 0:
        mmm = "0️⃣"
    elif num == 1:
        mmm = "1️⃣"
    elif num == 2:
        mmm = "2️⃣"
    elif num == 3:
        mmm = "3️⃣"
    elif num == 4:
        mmm = "4️⃣"
    elif num == 5:
        mmm = "5️⃣"
    elif num == 6:
        mmm = "6️⃣"
    elif num == 7:
        mmm = "7️⃣"
    elif num == 8:
        mmm = "8️⃣"
    elif num == 9:
        mmm = "9️⃣"
    return mmm
async def get_maps(message):
        conn = sqlite3.connect("db_map_vote.db")
        c = conn.cursor()
        class Map:
            def __init__(self, map_name, map_emoji,map_list,map_ratio):
                self.map_name = map_name
                self.map_emoji = map_emoji
                self.map_list = map_list
                self.map_ratio = map_ratio
        maps = []
        if c.execute('SELECT map_name, map_emoji, map_list from maps where Guild_ID == {0} and channel_id=={1}'.format(message.guild.id,message.channel.id)).fetchone() is None:
            await message.channel.send("No maps added!")
            return None
        else:
            rows = c.execute('SELECT map_name, map_emoji, map_list, Played_Times from maps where Guild_ID == {0} and channel_id=={1} order by map_list asc'.format(message.guild.id,message.channel.id)).fetchall()
        for i in rows:
            maps.append(Map(i[0], i[1], i[2],i[3]))
        return maps

client.run('') ##YOUR TOKEN HERE##
